CREATE TABLE Artist
(
	artist_id   INTEGER     NOT NULL      IDENTITY,
	phoneno     VARCHAR(50) NOT NULL,
	firstname   VARCHAR(50) NOT NULL,
	surname     VARCHAR(50) NOT NULL,
	special_requests      VARCHAR(500), 
	CONSTRAINT PK_Artist PRIMARY KEY (artist_id)
);

CREATE TABLE Type
(
	type_id		INTEGER     NOT NULL      IDENTITY,
	name		VARCHAR(50) NOT NULL,
	CONSTRAINT PK_Type PRIMARY KEY (type_id)
);

CREATE TABLE Performance
(
	performance_id		INTEGER     NOT NULL      IDENTITY,
	performance_name	VARCHAR(50) NOT NULL,
	performance_type	INTEGER		NOT NULL,
	CONSTRAINT PK_Performance PRIMARY KEY (performance_id),
	CONSTRAINT FK_Performance_Type FOREIGN KEY (performance_type) REFERENCES Type(type_id)
);

CREATE TABLE Venue
(
	venue_id	INTEGER     NOT NULL      IDENTITY,
	size		INTEGER		NOT NULL,
	CONSTRAINT PK_Venue PRIMARY KEY (venue_id)
);

CREATE TABLE Event
(
	event_id		INTEGER			NOT NULL      IDENTITY,
	event_name		VARCHAR(50)		NOT NULL,
	date			DATE			NOT NULL,
	price			DECIMAL(18,2)	NOT NULL,	
	venue_id		INTEGER			NOT NULL,
	CONSTRAINT PK_Event PRIMARY KEY (event_id),
	CONSTRAINT FK_Event_Venue FOREIGN KEY (venue_id) REFERENCES Venue(venue_id) 
 );

CREATE TABLE Booking
(
	booking_id		INTEGER			NOT NULL      IDENTITY,
	phoneno			VARCHAR(50),
	date			DATE			NOT NULL,
	seats			INTEGER			NOT NULL,
	is_paid			CHAR(1)	,
	event_id		INTEGER			NOT NULL,
	CONSTRAINT CHK_Booking_ispaid CHECK (is_paid IN ('Y','N', NULL)),
 	CONSTRAINT PK_Booking PRIMARY KEY (booking_id),
	CONSTRAINT FK_Booking_Event FOREIGN KEY (event_id) REFERENCES Event(event_id)
);

CREATE TABLE Performance_Artist
(
	performance_id	INTEGER			NOT NULL,
	artist_id		INTEGER			NOT NULL,
	CONSTRAINT PK_PA_Performance PRIMARY KEY (performance_id, artist_id),
    CONSTRAINT FK_PA_Performance FOREIGN KEY (performance_id) REFERENCES Performance(performance_id),
	CONSTRAINT FK_PA_Artist FOREIGN KEY (artist_id) REFERENCES Artist(artist_id)
);

CREATE TABLE Event_Performance
(
	event_id		INTEGER			NOT NULL,
	performance_id	INTEGER			NOT NULL,
    CONSTRAINT PK_EP_Event PRIMARY KEY (event_id, performance_id),
    CONSTRAINT FK_EP_Event FOREIGN KEY (event_id) REFERENCES Event(event_id),
	CONSTRAINT FK_EP_Performance FOREIGN KEY (performance_id) REFERENCES Performance(performance_id)
);

-- delete table 
/* Drop TABLE Event_Performance
DROP TABLE Performance_Artist
DROP TABLE Booking
DROP TABLE Event 
DROP TABLE Venue 
DROP TABLE Performance 
DROP TABLE Type
DROP TABLE Artist  */


-- Create access to other group members

--creat role

CREATE ROLE friend_role_18;

-- give the access to the role

GRANT SELECT, INSERT, DELETE ON Artist TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Type TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Performance TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Venue TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Event TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Booking TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Event_Performance TO friend_role_18;
GRANT SELECT, INSERT, DELETE ON Performance_Artist TO friend_role_18;

-- add user to the role

ALTER ROLE friend_role_18 ADD MEMBER BIT_SWD03_28;
ALTER ROLE friend_role_18 ADD MEMBER BIT_SWD03_22;