--Data insert into Artist

INSERT INTO Artist(phoneno, firstname, surname, special_requests) VALUES
('+358441234567', 'Arja', 'Raatikainen','Lactose intolerant food'),
('+441132980900', 'Big', 'Toe','Whiskey and chocolate'),
('+358509876543', 'Arttu', 'Wiskari','Five bottles of Coca-Cola'),
('+35890457863', 'Sofia', 'Merilainen','Tee')

INSERT INTO Artist(phoneno, firstname, surname) VALUES
('+358405678999', 'Arja', 'Koriseva'),
('+3584578945615', 'ZZ', 'Top'),
('+443524689712', 'Hoang', 'Mang')

SELECT * FROM Artist


--Data insert into Type

INSERT INTO Type(name) VALUES
('Theatre'),
('Concert'),
('Dance'),
('Other')

SELECT * FROM Type

--Data insert into Performance

INSERT INTO Performance(performance_name, performance_type) VALUES
('Ego', 3),
('K-Pop remix', 2),
('Do not say goodbye', 3),
('Brown sugar',1),
('Kulkuset', 2),
('Kuningaslohi', 1),
('Oh year-Monalisa',4)

SELECT * FROM Performance;
UPDATE Performance SET performance_type = 1 WHERE performance_id = 5


--Data insert into Venue

INSERT INTO Venue(size) VALUES
(50),
(500)

SELECT * FROM Venue;


--Data insert into Performan_Artist

INSERT INTO Performance_Artist(performance_id, artist_id) VALUES
(1,1),
(2,2),
(2,7),
(3,4),
(3,7),
(4,6),
(5,5),
(6,3),
(7,4)

SELECT * FROM Performance_Artist ORDER BY performance_id


--Data insert into Event

INSERT INTO Event(event_name, date, price, venue_id) VALUES
('Nightwish concert', '2016-12-15', 30.00, 2),
('New Year dance show', '2017-01-04', 23.50, 2),
('Holiday Spirit', '2016-12-20', 49.99, 1),
('Xmas theatre', '2016-12-21', 14.99, 2),
('New Year carol concert', '2016-12-31', 9.99, 2),
('Xmas theatre', '2017-01-08', 39.99, 1)

SELECT * FROM Event;
-- SELECT Performance.performance_id,performance_name,Artist.artist_id,firstname,surname, name FROM Performance JOIN Performance_Artist ON (Performance_Artist.performance_id =Performance.performance_id) JOIN Artist ON (Artist.artist_id =Performance_Artist.artist_id) JOIN Type ON (Performance.performance_type = Type.type_id) ORDER BY name

--Data insert into Event_Performance

INSERT INTO Event_Performance(event_id,performance_id) VALUES
(1,2),
(1,5),
(2,1),
(2,3),
(2,6),
(3,7),
(4,5),
(4,6),
(4,4),
(5,2),
(5,1),
(5,3),
(5,4),
(6,4),
(6,5)

SELECT * FROM Event_Performance;
-- SELECT * FROM Event JOIN Event_Performance ON (Event.event_id = Event_Performance.event_id) JOIN Performance ON (Performance.performance_id = Event_Performance.performance_id) 


--Data insert into Booking

INSERT INTO Booking(phoneno, date, seats, is_paid, event_id) VALUES
(04012345678,'2016-12-11', 2, 'Y', 1),
(04012345679,'2016-12-11', 10, 'Y', 1),
(04012356878,'2016-12-09', 5, 'N', 1),
(04012345680,'2016-12-14', 5, 'Y', 4),
(04012345681,'2016-12-11', 8, 'Y', 3),
(04012399679,'2016-12-10', 3, 'N', 1),
(04012345682,'2016-12-16', 1, 'Y', 6),
(04012345683,'2016-12-11', 36, 'Y', 2),
(04012345684,'2016-12-11', 3, 'Y', 5),
(04012345685,'2016-12-12', 11, 'Y', 4),
(04012345686,'2016-12-10', 2, 'Y', 1),
(04012345689,'2016-12-11', 6, 'Y', 3),
(04012342680,'2016-12-10', 2, 'N', 4),
(04012345691,'2016-12-13', 9, 'Y', 1),
(04012345693,'2016-12-11', 23, 'Y', 2),
(04012345699,'2016-12-10', 16, 'Y', 5)

INSERT INTO Booking(phoneno, date, seats,  event_id) VALUES
(04012345789,'2016-12-11', 13,  2),
(04012345561,'2016-12-12', 10,  1),
(04012345888,'2016-12-10', 5,  4),
(04012345354,'2016-12-11', 4,  3),
(04012345432,'2017-01-02', 14,  6),
(04012345438,'2017-01-01', 36, 2),
(04012345123,'2016-12-1', 3, 5),
(04012345889,'2016-12-10', 12, 1)

SELECT * FROM Booking