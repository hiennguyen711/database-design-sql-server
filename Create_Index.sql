--Create index for Event FK
CREATE INDEX FK_venue_id ON Event(venue_id)

--Create index for Booking FK
CREATE INDEX FK_event_id ON Booking(event_id)

--Create index for Performance

CREATE INDEX FK_performance_type ON Performance(performance_type)

CREATE UNIQUE INDEX UQ_performance_name ON Performance(performance_name)

--Create index for Performance_Artist

CREATE INDEX FK_performance_id ON Performance_Artist(performance_id)

CREATE INDEX FK_artist_id ON Performance_Artist(artist_id)

--Create index for Event_Performance

CREATE INDEX FK_event_id ON Event_Performance(event_id)

CREATE INDEX FK_performance_id ON Event_Performance(performance_id)