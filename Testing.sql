--1. What events are coming up next month? 
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT event_name 
	FROM Event 
	WHERE MONTH(date) = MONTH (DATEADD (month, 1, CAST(GETDATE() AS DATE)))
		AND YEAR(date) = YEAR (DATEADD (month, 1, CAST(GETDATE() AS DATE)));
COMMIT;

--2. What theatre performances are coming up this month?
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT DISTINCT performance_name 
	FROM Performance
	JOIN Event_Performance ON (Event_Performance.performance_id = Performance.performance_id)
	JOIN Event ON (Event_Performance.event_id = Event.event_id)
	JOIN Type ON (Performance.performance_type = Type.type_id) 
	WHERE name = 'Theatre' AND MONTH(date) = MONTH (DATEADD (month, 1, CAST(GETDATE() AS DATE)))
		AND YEAR(date) = YEAR (DATEADD (month, 1, CAST(GETDATE() AS DATE)));
COMMIT;

--3. What is Arja Koriseva's phone number?
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT phoneno 
	FROM Artist 
	WHERE firstname = 'Arja' AND surname = 'Koriseva';
COMMIT;

--4. When will ZZ Top3 perform in Kuru and what are their special requests for catering?
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT Event.date, (CASE WHEN special_requests IS NULL THEN ' No special requests ' ELSE special_requests END ) AS Special_request
	FROM Event 
		JOIN Event_Performance ON (Event.event_id =Event_Performance.event_id)
		JOIN Performance_Artist	ON (Event_Performance.performance_id = Performance_Artist.performance_id)
		JOIN Artist ON (Performance_Artist.artist_id =Artist.artist_id)
		WHERE firstname= 'ZZ' AND surname = 'Top';
COMMIT;

--5. How many tickets have been sold to Arja Raatikainen's dance performance 'Ego' that takes place on 4.1.2017?
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT SUM(seats) AS solded_tickets 
	FROM Booking 
		JOIN Event ON (Booking.event_id = Event.event_id)
		JOIN Event_Performance ON (Event.event_id =Event_Performance.event_id)
		JOIN Performance ON (Performance.performance_id =Event_Performance.performance_id)
		JOIN Performance_Artist	ON (Event_Performance.performance_id = Performance_Artist.performance_id)
		JOIN Artist ON (Performance_Artist.artist_id =Artist.artist_id)
		WHERE firstname= 'Arja' AND surname = 'Raatikainen' AND performance_name = 'Ego' AND Event.date = '2017-01-04' AND is_paid != 'N';
COMMIT;

--6. How many tickets are there left to Nightwish concert on 12.12.2016?
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT  (size - sum (seats) )AS left_tickets
	FROM Booking 
		JOIN Event ON (Booking.event_id = Event.event_id)
		JOIN Venue ON (Venue.venue_id = Event.venue_id)
		WHERE event_name = 'Nightwish concert' AND Booking.date <= '2016-12-12' AND is_paid != 'N'
		GROUP BY size;
COMMIT;

--7. How much money has the Kuru Culture Club got from sold tickets this year?
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
BEGIN TRANSACTION;
SELECT  sum(price*seats )AS "this year total revenue "
	FROM Booking
	JOIN Event ON (Booking.event_id = Event.event_id)  
		WHERE Booking.date <= '2016-12-31'  AND Booking.date >= '2016-01-01' AND is_paid != 'N';	
COMMIT;